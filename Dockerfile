FROM ubuntu:14.04

MAINTAINER Dennis Hazelett "dennis.hazelett@csmc.edu"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
	apt-get install wget git

COPY hello.sh /usr/bin/hello

WORKDIR /usr/bin

RUN chmod +x /usr/bin/hello

VOLUME /data

CMD ["hello"]

